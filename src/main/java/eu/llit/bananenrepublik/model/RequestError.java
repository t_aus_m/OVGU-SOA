package eu.llit.bananenrepublik.model;

public class RequestError extends RuntimeException {
    
    public class Info {
        public Info(String message) { this.error = message; }
        public String error;
    }
    public Info detail;
    
    public RequestError(String message) {
        this.detail = new Info(message);
    }
    
}
