package eu.llit.bananenrepublik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Database {
    private static Connection instance;
    private static void init() {
        try {
            Class.forName("org.sqlite.JDBC");
            instance = DriverManager.getConnection("jdbc:sqlite:test.db");

            Statement stmt;
            
            stmt = instance.createStatement();
        	stmt.executeUpdate("CREATE TABLE IF NOT EXISTS bestellung (count INT, address TEXT, ccNumber TEXT, ccName TEXT, ccValidity INT, ccCVC INTN, id INT, priceTotal INT, status TEXT)");
        
            stmt = instance.createStatement();
        	stmt.executeUpdate("CREATE TABLE IF NOT EXISTS review (rating FLOAT, content TEXT)");
        } catch (Exception ex) {
            System.out.println(ex);
            System.exit(2);
        }
    }

    private Database(){}
    public static Connection get() {
        if (instance == null) init();
        return instance;
    }
}
