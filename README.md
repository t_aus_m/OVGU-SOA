# Bananenrepublik
Ein API-Webshop für Bananen.

> Übung 2b für Service Engineering  
> Till Riechard & Moritz Marquardt

### ➜ [Swagger-Dokumentation](https://bananenrepublik-1.api-docs.io/1.0.0/)

## Entwicklung & Ausführung
Alles was man braucht ist das [OpenJDK 8](https://openjdk.java.net/projects/jdk8/) oder eine kompatible Version des JDK. Für ein Deployment wird zusätzlich [Docker](https://www.docker.com/) benötigt.  
Als IDE verwenden wir [VSCodium](https://vscodium.com/) mit der `redhat.java`-Erweiterung für die Java-Entwicklung, sowie `pivotal.vscode-spring-boot` für bessere Syntax-Vervollständigung und `arjun.swagger-viewer` zur einfacheren Erstellung der API-Dokumentation. Unser Versionskontrollsystem ist [Git](https://git-scm.com/).

Das Repository kann mit `git clone https://codeberg.org/momar/ovgu-soa.git && cd ovgu-soa` heruntergeladen werden; mit `./mvnw install` können nun alle benötigten Dependencies installiert werden.

Mit `./mvnw spring-boot:run` kann die Anwendung jetzt im Entwicklungsmodus gestartet werden, oder mit `./mvnw package` als JAR-Datei exportiert werden.

### Deployment
Mittels `docker build -t api2b .` wird ein Docker-Image mit dem Namen "api2b" gebaut, welches nun mit `docker run --rm -p 8080:8080 -v "$PWD:/data" api2b` ausgeführt werden kann - die Datenbank wird dabei als `test.db` abgelegt.

## Client
Die Datei `client.js` beinhaltet einen JavaScript-Client für die API, und kann beispielsweise mit [node.js](https://nodejs.org/en/) ausgeführt werden - dafür wird [`node-fetch`](https://www.npmjs.com/package/node-fetch) benötigt (kann mit `npm install node-fetch` installiert werden), danach kann der Client als REPL über `node -i --experimental-repl-await -r ./client.js` ausgeführt werden.

### Beispiel

```javascript
> await banana.info()
{ title: 'Banane',
  description: 'Banane',
  price: 500,
  averageRating: 0,
  reviews: [] }
> await banana.review(0.9, "Tolle Banane!")
{ error: null }
> await banana.review(0.7, "Hab schon bessere Bananen gegessen...")
{ error: null }
> (await banana.info()).reviews
[ { rating: 0.9, content: 'Tolle Banane!' },
  { rating: 0.7,
    content: 'Hab schon bessere Bananen gegessen...' } ]
> await banana.order(5)
Thrown:
Error: Ihre Kreditkarte ist nicht für Bananen freigeschaltet.
    at Object.handleError (/home/momar/Uni/Service Engineering/api2b/client.js:5:130)
    at process._tickCallback (internal/process/next_tick.js:68:7)
> await banana.order(10, "Otto-von-Guericke-Universität Magdeburg\nUniversitätsplatz 2\n39106 Magdeburg", {number:"4242424242424242", name:"Otto von Guericke", validity:202307, cvc:456})
{ id: 1, totalPrice: 5000 }
> await banana.status(1)
{ count: 10,
  pricePerBanana: 500,
  priceTotal: 5000,
  status: 'awaiting-payment' }
> (await banana.status(1)).status
'growing'
> (await banana.status(1)).status
'packing'
> (await banana.status(1)).status
'shipping'
> (await banana.status(1)).status
'eaten'
```
