package eu.llit.bananenrepublik;

import javax.validation.Valid;

import eu.llit.bananenrepublik.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@RestController
@RequestMapping("/v1")
public class Controller {

    @RequestMapping(value = "/banane", method = RequestMethod.GET)
    public Banane getBanane() {
        Banane b = new Banane();
        return b;
    }

    @RequestMapping(value = "/banane", method = RequestMethod.POST)
    public EmptyResponse rateBanane(@Valid @RequestBody Review body) throws Exception {
        body.publish();
        return new EmptyResponse();
    }

    @RequestMapping(value = "/bestellung", method = RequestMethod.POST)
    public BestellungInfo createBestellung(@Valid @RequestBody Bestellung body) throws Exception {
        return body.place();
    }

    @RequestMapping(value = "/bestellung/{id}", method = RequestMethod.GET)
    public Status getBestellung(@PathVariable("id") Integer id) throws Exception {
        return new Status(id);
    }

    @ExceptionHandler(RequestError.class)
    public final ResponseEntity<RequestError.Info> handleRequestErrorError(RequestError ex, WebRequest request) {
        return new ResponseEntity<>(ex.detail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundError.class)
    public final ResponseEntity<NotFoundError.Info> handleNotFoundError(NotFoundError ex, WebRequest request) {
        return new ResponseEntity<>(ex.detail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PaymentError.class)
    public final ResponseEntity<PaymentError.Info> handlePaymentError(PaymentError ex, WebRequest request) {
        return new ResponseEntity<>(ex.detail, HttpStatus.PAYMENT_REQUIRED);
    }

}
