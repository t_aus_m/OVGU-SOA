package eu.llit.bananenrepublik.model;

import java.sql.PreparedStatement;
import java.sql.Statement;

import eu.llit.bananenrepublik.Database;

public class Bestellung {

	public int count;
	public String address;
	public String ccNumber;
	public String ccName;
	public int ccValidity;
	public int ccCVC;


    public BestellungInfo place() throws Exception {
		if (!this.ccNumber.equals("4242424242424242")) throw new PaymentError("Ihre Kreditkarte ist nicht für Bananen freigeschaltet.");

		Statement stmt = Database.get().createStatement();
		int lastId = stmt.executeQuery("SELECT MAX(id) FROM bestellung").getInt(1);
		stmt.close();

		PreparedStatement prep = Database.get().prepareStatement("INSERT INTO bestellung VALUES (?,?,?,?,?,?,?,?,?);");
		prep.setInt(1, this.count);
		prep.setString(2, this.address);
		prep.setString(3, this.ccNumber);
		prep.setString(4, this.ccName);
		prep.setInt(5, this.ccValidity);
		prep.setInt(6, this.ccCVC);
		prep.setInt(7, lastId + 1);
		prep.setInt(8, new Banane().price * this.count);
		prep.setString(9, "awaiting-payment");
		prep.execute(); prep.close();

		return new BestellungInfo(lastId +1, new Banane().price * this.count);
    }

}
