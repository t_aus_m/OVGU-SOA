package eu.llit.bananenrepublik.model;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import eu.llit.bananenrepublik.Database;

import java.util.ArrayList;

public class Banane {
	public String title;
	public String description;
	public int price;
	public float averageRating;
	public List<Review> reviews;

	public Banane() {
		title = "Banane";
		description = "Banane";
		price = 500;

		reviews = new ArrayList<Review>();

		try {
			Statement stmt;
			
			stmt = Database.get().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM review");
			while (rs.next()) reviews.add(new Review(rs.getFloat("rating"), rs.getString("content")));
			stmt.close();

			stmt = Database.get().createStatement();
			averageRating = stmt.executeQuery("SELECT AVG(rating) FROM review").getFloat(1);
			stmt.close();
		} catch (Exception ex) { System.out.println(ex); }
	}

}
