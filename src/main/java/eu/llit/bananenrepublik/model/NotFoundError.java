package eu.llit.bananenrepublik.model;

public class NotFoundError extends RuntimeException {

    public class Info {
        public Info(String message) { this.error = message; }
        public String error;
    }
    public Info detail;

    public NotFoundError(String message) {
        this.detail = new Info(message);
    }

}
